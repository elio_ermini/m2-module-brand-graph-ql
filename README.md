## BrandGraphQl

Magento 2.4 module that extends the products GraphQL with a new type and resolver information for fields for retrieving brand information.
## Installation

* Go to your installation directory of Magento 2 and perform the following commands
* `composer config repositories.m2-module-brand-graph-ql vcs https://elio_ermini@bitbucket.org/elio_ermini/m2-module-brand-graph-ql.git `
* `composer require ermini/m2-module-brand-graphql`
* `php bin/magento setup:upgrade`
* `php bin/magento setup:di:compile`
* `php bin/magento cache:clean`

## Documentation

If default settings are chosen, the product attribute with code "brand" and type select is required to be already existing or added new.

GraphQL Brand schema is composed of 2 fields:

- code: Value of the Brand in admin area
- value: Value of the Brand for current store view

Graphql endpoint is Magento 2 default: https://magento.example/graphql

### Query format

GraphQL request for name,sku and brand information:

```graphql
{
    products(search: "Yoga bag", pageSize: 2) {
        items {
            name,
            sku,
            brand {
                code,
                value
            }
        }
    }
}
```
#### Headers
Adding the header "Store" with value [store_view_code] will affect the response of the field "value" for that store view code.

### Settings

The default attribute has code "brand" however this can be changed among the possible select/dropdown attributes in 2 ways:

in the admin area under:

**Stores -> Configuration -> Ermini -> Brand GraphQL -> Settings**

![Alt text](docs/settings.png "settings")


or via CLI:

``bin/magento config:set ermini_brand_graphql/settings/brand_attribute_code "an_attribute_code"``

To save the setting the app/etc/env.php file add -l suffix, to save in the app/etc/config.php file add -l suffix.

### Magento Version Support

The module was developed for and requires Magento 2.4 and PHP 7.4.

### Tests

Unit tests can be run with:

``php vendor/bin/phpunit -c dev/tests/unit/phpunit.xml.dist vendor/ermini/m2-module-brand-graph-ql/Test/Unit/``

### Examples
The following screenshot demonstrate the behaviour in a store with multiple store views (DE and FR).
#### Stores setup
![Alt text](docs/stores.png "stores")
#### Brand attribute setup
![Alt text](docs/brand.png "brand")
The following GraphQL examples are performed inside the Chrome browser exension "Altair GraphQL Client"
#### Example request in the default store (DE)
![Alt text](docs/example-de.png "example-de")
#### Example adding headers Store = fr
![Alt text](docs/headers.png "headers fr")
![Alt text](docs/example-fr.png "example fr")
