<?php
declare(strict_types=1);

namespace Ermini\BrandGraphQl\Test\Unit\Model;

use Ermini\BrandGraphQl\Model\Config;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class ConfigTest extends TestCase
{
    private const XML_PATH_BRAND_ATTRIBUTE_CODE = 'ermini_brand_graphql/settings/brand_attribute_code';

    /**
     * @var ObjectManager
     */
    private $objectManager;

    /**
     * @var Config
     */
    private $config;

    /**
     * @var ScopeConfigInterface|MockObject
     */
    private $scopeConfigMock;

    /**
     * @inheirtDoc
     */
    protected function setUp(): void
    {
        $this->objectManager = new ObjectManager($this);

        $this->scopeConfigMock = $this->getMockForAbstractClass(ScopeConfigInterface::class);

        $this->config = $this->objectManager->getObject(
            Config::class,
            [
                'scopeConfig' => $this->scopeConfigMock
            ]
        );
    }

    /**
     * Get brand attribute from configuration test.
     */
    public function testGetBrandAttributeCode(): void
    {
        $this->scopeConfigMock->expects($this->once())
            ->method('getValue')
            ->with(self::XML_PATH_BRAND_ATTRIBUTE_CODE)
            ->willReturn('brand');
        $this->assertEquals('brand', $this->config->getConfigBrandAttributeCode());
    }
}
