<?php

namespace Ermini\BrandGraphQl\Model\Config\Source;

use Magento\Catalog\Model\ResourceModel\Eav\AttributeFactory;
use Magento\Framework\Data\OptionSourceInterface;
use Magento\Framework\Exception\LocalizedException;

class Brand implements OptionSourceInterface
{
    const PRODUCT_ENTITY_TYPE_ID = '4';
    const ATTRIBUTE_TYPE = 'select';

    private AttributeFactory $attributeFactory;

    public function __construct(AttributeFactory $attributeFactory)
    {
        $this->attributeFactory = $attributeFactory;
    }

    /**
     * Returns the product attributes that are type select.
     *
     * @inheritdoc
     */
    public function toOptionArray()
    {
        $options = [];

        $attribute = $this->attributeFactory->create();

        try {
            $collection = $attribute->getResourceCollection();
        } catch (LocalizedException $e) {
            return [];
        }

        $collection->addFieldToSelect('attribute_code');
        $collection->addFieldToSelect('frontend_label');
        $collection->addFieldToFilter('entity_type_id', ['eq' => self::PRODUCT_ENTITY_TYPE_ID]);
        $collection->addFieldToFilter('frontend_input', ['eq' => self::ATTRIBUTE_TYPE]);
        $collection->setOrder('frontend_label', 'asc');

        $items = $collection->getItems();

        foreach ($items as $option) {
            $options[] = [
                'value' => $option->getData('attribute_code'),
                'label' => __($option->getData('frontend_label'))
            ];
        }

        return $options;
    }
}
