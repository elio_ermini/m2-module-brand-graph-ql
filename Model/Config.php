<?php

namespace Ermini\BrandGraphQl\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;

class Config
{
    private const XML_PATH_BRAND_ATTRIBUTE_CODE = 'ermini_brand_graphql/settings/brand_attribute_code';

    private ScopeConfigInterface $scopeConfig;

    public function __construct(ScopeConfigInterface $scopeConfig)
    {
        $this->scopeConfig = $scopeConfig;
    }

    public function getConfigBrandAttributeCode(): string
    {
        return $this->scopeConfig->getValue(self::XML_PATH_BRAND_ATTRIBUTE_CODE);
    }
}
