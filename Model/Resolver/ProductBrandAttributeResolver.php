<?php
declare(strict_types=1);

namespace Ermini\BrandGraphQl\Model\Resolver;

use Ermini\BrandGraphQl\Model\Config;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\CollectionFactory as AttributeOptionFactory;
use Magento\Framework\DataObject;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Framework\GraphQl\Config\Element\Field;

class ProductBrandAttributeResolver implements ResolverInterface
{

    const ADMIN_ATTRIBUTE_VALUE = 'default_value';
    const FRONTEND_ATTRIBUTE_VALUE = 'store_default_value';

    private Config $config;
    private ProductRepositoryInterface $productRepository;
    private AttributeOptionFactory $attributeOptionCollectionFactory;

    public function __construct(
        Config                     $config,
        ProductRepositoryInterface $productRepository,
        AttributeOptionFactory     $attributeOptionCollection
    ) {
        $this->config = $config;
        $this->productRepository = $productRepository;
        $this->attributeOptionCollectionFactory = $attributeOptionCollection;
    }

    /**
     * @inheirtDoc
     */
    public function resolve(Field $field, $context, ResolveInfo $info, array $value = null, array $args = null)
    {
        if (!array_key_exists('model', $value) || !$value['model'] instanceof ProductInterface) {
            throw new LocalizedException(__('"model" value should be specified'));
        }

        /* @var $product ProductInterface */
        $product = $value['model'];

        $attributeCode = $this->config->getConfigBrandAttributeCode();

        if (!$product->hasData($attributeCode)) {
            $product = $this->productRepository->getById($product->getId());
        }

        if (!$product->hasData($attributeCode)) {
            return [];
        }

        if (!$attributeId = $product->getData($attributeCode)) {
            return [];
        }

        if (!$optionData = $this->getBrandAttributeData((int)$attributeId)) {
            return [];
        }

        return [
            'code' => $optionData->getData(self::ADMIN_ATTRIBUTE_VALUE),
            'value' => $optionData->getData(self::FRONTEND_ATTRIBUTE_VALUE)
        ];
    }

    private function getBrandAttributeData(int $attributeId): DataObject
    {
        return $this->attributeOptionCollectionFactory->create()
            ->setPositionOrder('asc')
            ->setIdFilter($attributeId)
            ->setStoreFilter()
            ->getFirstItem();
    }
}
